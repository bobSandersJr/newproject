# Salesforce Package Creator for Unix Systems

## Prerequisites

- ant already setup

- know the directory with you build.xml file in it

- have node installed

- have the commands setup to run Sublime or VS Code (subl or code) from the command line, a quick google seach can help you with this.

## Install

After cloning this repo down to your machine, navigate into the repo. Run the command `node install.js`

After that run the command `new-project -v`, if you did not get a semantic version number, something went wrong

## Options

- `--version`, `-v` gives the current semantic version number

- `--help`, `-h` lists all options

- `--project`, `-p` denotes the name of the project you would like to create, required

- `--root`, `-r` denotes the path of the folder you would like to save the project in, defaults to '/Salesforce'. This folder should contain your build.xml files.

- `--editor`, `-e` boolean argument to change editor that opens the project from VS Code to Sublime

## Notes

The act of creating a project prepends the "rm " prefix. For example, if you ran `new-project -p BUG1234` the file created would be "rm BUG1234"

I plan to impliment a .env file in the future to be able to set person preferences (rm prepending, root directory, default editor)

I plan to make this cross compatible with Windows in the future.