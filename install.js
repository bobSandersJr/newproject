/**
 * npm packages
 */
const { exec } = require('child_process');
const fs = require('fs');
const os = require('os');

exec('npm install');

const path = require('path');
const _ = require('lodash');

console.log('copying files...\n')
const location = '/usr/local/share/newProject';
const locationFiles = fs.readdirSync('/usr/local/share');
if(!_.includes(locationFiles, 'newProject')){
  fs.mkdirSync(location);
}
fs.copyFileSync(path.join(__dirname, 'newProject.js'), `${location}/newProject.js`);
fs.copyFileSync(path.join(__dirname, 'package.json'), `${location}/package.json`);
fs.copyFileSync(path.join(__dirname, 'package.xml'), `${location}/package.xml`);

console.log('verifying files...\n')
process.chdir(location);

const files = fs.readdirSync(location);
if(files.length < 3){
  console.log('copy failed...\n');
}
else if(files.length === 3){
  console.log('copy successful...\n');
  exec('npm install');
}

process.chdir(__dirname);
const homeFiles = fs.readdirSync(`/Users/${os.userInfo().username}`);
if(!_.includes(homeFiles, '.bash_profile')){
  console.log('creating bash profile...\n');
  fs.copyFileSync(path.join(__dirname, '.bash_profile'), `/Users/${os.userInfo().username}/.bash_profile`)
}
else{
  console.log('adding alias to bash profile...\n');
  try{
    fs.appendFileSync(`/Users/${os.userInfo().username}/.bash_profile`, "alias new-project='node /usr/local/share/newProject/newProject.js'\n");
    console.log('alias appended...');
  }
  catch(e){
    console.log(e);
  }
}

exec(`source /Users/${os.userInfo().username}/.bash_profile`);