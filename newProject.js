/**
 * require npm modules
 */
const yargs = require('yargs');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const { exec } = require('child_process');


 /**
  * helper functions
  */

/**
 * yargs setup
 */
const argv = yargs
  .option('project', {
    alias: 'p',
    describe: 'name of the project to create',
    type: 'string'
  })
  .option('root', {
    alias: 'r',
    describe: 'folder that holds created project',
    type: 'string',
    default: '/Salesforce'
  })
  .option('editor', {
    alias: 'e',
    describe: 'flag to use sublime text editor to open project',
    type: 'boolean'
  })
  .demandOption(['project'], 'You must provide a project name')
  .help()
  .alias('h', 'help')
  .alias('v', 'version')
  .argv

  /**
   * program logic
   */

  const rootContents = fs.readdirSync(argv.r);
  const prefix = argv.p.substring(0,3);
  const projectName = prefix === 'rm ' ? argv.p : `rm ${argv.p}`;
  
  console.log('Checking if the project folder alread exists...\n')
  if(!_.includes(rootContents, projectName)){
    console.log('Making project folder...');
    fs.mkdirSync(`${argv.r}/${projectName}`);
  }

  const projectContents = fs.readdirSync(`${argv.r}/${projectName}`);
  if(!_.includes(projectContents, 'package.xml')){
    fs.copyFileSync(path.join(__dirname, '/package.xml'),`${argv.r}/${projectName}/package.xml`);
  }

  process.chdir(`${argv.r}/${projectName}/`)
  if(argv.e==true){
    setTimeout(() => {
      exec('subl .');
    }, 0);
  }
  else{
    exec('code .');
  }
